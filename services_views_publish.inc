<?php

function services_views_publish_execute_view($view_info, $view = NULL, $display_id = NULL) {
  if (!empty($view_info)) {
    $view_name = $view_info['view_name'];
    $display_id = $view_info['display_id'];

    $args = array();

    $view = views_get_view($view_name);
    $view->set_arguments($args);
  }

  // Avoid translation of the field labels.
  $view->localization_plugin = views_get_plugin('localization', 'none');

  // Execute a view.
  $output = $view->render($display_id);
  $output = array();
  // Add support for Services Publish formatter.
  // array_push ( $output , $view->result );
  // array_push ( $output , $view->field );
  /*
  foreach ( $view->field as $field_index => $field ) {
    if ( strpos ( $field_index, 'field_' ) === false ) {
          $field_structure = $view->field[$field_index];
          
          //array_push ( $output , $field_structure );
          //array_push ( $output , $view->result );
          
          $allowed_values = isset($field_structure->field_info['settings']['allowed_values'])?$field_structure->field_info['settings']['allowed_values']: '';
          $description = isset($field_structure->definition['help'])?$field_structure->definition['help']:'';
          $field_type = isset($field_structure->field_info['columns']['target_id']['type'])?$field_structure->field_info['columns']['target_id']['type']:'';
          $field_output = array ( 
                                   'machine_id' => $field_structure->field_alias,
                                   'field_id' => $field_structure->field_alias, 
                                   'field_label' => $field_structure->options['label'],
                                   //'field_type' => $field_structure->options['type'],
                                   'field_description' => $description,
                                   'field_input_type' => $field_type,
                                   //'allowed_values' => $allowed_values,
                                   //'field_default_value' => '',
                                   'field_value' => $view->result[$field_structure->field_alias],
                                );
          $i = 0;
          foreach ( $view->result as $result ) {
            if ( !is_array($output[$i] ) ) {
              $output[$i] = array();
            }
            array_push ( $output[$i++] , $field_output );
          }
    }
  }*/
  $i = 0;
  foreach ( $view->result as $result ) {
      $output[$i] = array();
      foreach ( $result as $field_index => $field ) {
            if ( strpos ( $field_index, 'field_' ) === false ) {
                  $field_output = array ( );
                  $field_id = str_replace ( 'field_' , '' , $field_index );
                  $field_structure = $view->field[$field_id];
                  
                  //array_push ( $output , $field_structure );
                  //array_push ( $output[$i] , $field_structure );
                  
                  $allowed_values = isset($field_structure->field_info['settings']['allowed_values'])?$field_structure->field_info['settings']['allowed_values']: '';
                  $description = isset($field_structure->definition['help'])?$field_structure->definition['help']:'';
                  $field_type = isset($field_structure->field_info['columns']['target_id']['type'])?$field_structure->field_info['columns']['target_id']['type']:'';
                  
                  //$field_output['field_data'] = $result;
                  
                  $field_output['machine_id'] = $field_index;
                                    
                  if ( isset($field_structure->field) ) { 
                    $field_output['field_id'] = $field_structure->field;
                  }
                  
                  if ( isset($field_structure->options['label']) ) {
                    $field_output['field_label'] = $field_structure->options['label'];
                  }
                  
                  if ( isset($field_structure->real_field) ) {
                    $field_output['field_type'] = $field_structure->real_field;
                  }
                  
                  if ( !empty($description) ) {
                    $field_output['field_description'] = $description;
                  }
                  
                  if ( !empty($field_type) ) {
                    $field_output['field_input_type'] = $field_type;
                  }
                  
                  if ( strpos ( $field_index , 'picture' ) !== false ) {
                    $field_output['uri'] = file_load($field)->uri;
                  }                  
                  
                  $field_output['field_value'] = $field;    
                                                       
                  // Check html_strip property
                  if ($field_structure->options['alter']['strip_tags'] == '1') {
                    // Strip the tags which aren't allowed using strip_tags ( $subject, $preserved_tags ).
                    $field_output['field_value'] = strip_tags($field_output['field_value'] , $field_structure->options['alter']['preserve_tags']);
                  }
                                        
                  array_push ( $output[$i] , $field_output );                            
            } else
            if ( strpos ( $field_index , 'field_field_' ) !== false ) {
                  $field_output = array ( );
                  //Grab key for field structure
                  $field_id = str_replace ( 'field_field_' , 'field_' , $field_index );
                  $field_structure = $view->field[$field_id];
                  
                  
                  
                  //array_push ( $output , $field_structure );
                  //array_push ( $output , $field );
                  
                  $allowed_values = isset($field_structure->field_info['settings']['allowed_values'])?$field_structure->field_info['settings']['allowed_values']: '';
                  $description = isset($field_structure->field_info['columns']['target_id']['description'])?$field_structure->field_info['columns']['target_id']['description']:'';
                  $field_type = isset($field_structure->field_info['columns']['target_id']['type'])?$field_structure->field_info['columns']['target_id']['type']:'';
                  $field_type = $field_structure->field_info['field_name'];
                  
                  $field_output['machine_id'] = $field_structure->options['id'];
                  $field_output['field_id'] = $field_structure->field_info['id'];
                  $field_output['field_label'] = $field_structure->options['label'];
                  $field_output['field_type'] = $field_structure->options['type'];
                  $field_output['field_description'] = $description;
                  $field_output['field_input_type'] = $field_type;
                  $field_output['allowed_values'] = $allowed_values;
                  //$field_output['field_default_value'] = $view->field;
                  $field_output['field_value'] = $field[0]['rendered']['#markup'];                      
                  
                  // Check html_strip property
                  if ($field_structure->options['alter']['strip_tags'] == '1') {
                    // Strip the tags which aren't allowed using strip_tags ( $subject, $preserved_tags ).
                    $field_output['field_value'] = strip_tags($field_output['field_value'] , $field_structure->options['alter']['preserve_tags']);
                  }
                  
                  array_push ( $output[$i] , $field_output );
            }
          
      }
      $i++;
  }
  
  //hook
  drupal_alter('services_views_publish_execute_view', $output, $view);
  
  $view->destroy();
  return $output;
}
